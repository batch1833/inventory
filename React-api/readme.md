# Booking API Overview
## Initial Set-up
* Change the MongoDB Atlas database connection string in the backend to ensure connectivity to the correct personal database.

* Import the Capstone2-leguin.postman_collection.json in your postman application.

## Store the following data in your database
### User credentials (User Request > User Registration)
* Admin User
	* 	firstName : "joannes",
    *   lastName : "leguin",
    *   email : "joannesleguin@mail.com",
    *   password : joannesleguin1234
    *   createdOn : "2022-07-07T04:55:57.613Z",
    *   isAdmin : true,
 
* Customer User
	*	firstName : "jane",
    *   lastName : "janedoe",
    *   email : "janedoe@mail.com",
    *   password : janedoe1234
    *   createdOn : "2022-07-07T11:58:40.679Z",
    *   isAdmin : true,

### Product Creation (Course Request > Create a course)
* Product 1
	* productName : "Road bikes",
	* description : "Road bikes are bicycles designed to take you as far and as fast as your legs can manage on paved surfaces. The road bike gets its name from the terrain it is designed to be used on – the road.At first glance, the road bike hasn’t changed a great deal over the years. Compared to a complex full-suspension mountain bike, it’s a traditional bicycle with a near-identical silhouette to those raced 50 years ago.",
	* price : 10000,
	* stocks : 100,
	* isActive : true,
	* createOn : "2022-07-07T10:48:18.475Z",
* Product 2
	* productName : "Touring bikes",
    * description : "Touring bikes are bicycles designed to take you as far and as fast as your legs can manage on paved surfaces. The road bike gets its name from the terrain it is designed to be used on – the road.At first glance, the road bike hasn’t changed a great deal over the years. Compared to a complex full-suspension mountain bike, it’s a traditional bicycle with a near-identical silhouette to those raced 50 years ago.",
    * price : 10000,
    * stocks : 100,
        "isActive : true,
        "createOn": "2022-07-07T10:48:18.475Z",
* Product 3
	* productName : "Hybrid/Electronic bikes",
    * description : "Hybrid/Electronic bikes are bicycles designed to take you as far and as fast as your legs can manage on paved surfaces. The road bike gets its name from the terrain it is designed to be used on – the road.At first glance, the road bike hasn’t changed a great deal over the years. Compared to a complex full-suspension mountain bike, it’s a traditional bicycle with a near-identical silhouette to those raced 50 years ago.",
    * price : 10000,
    * stocks : 100,
    * isActive : true,
    * createOn : "2022-07-07T11:58:40.747Z",

### Booking API Features
* User register (user/admin)
* User login (user/admin)
* User checkout/order
* User get my orders
* Items get all active products (admin/user)
* Items get specific products (admin/user)
* Admin Retrieve all users (Admin only)
* Admin Create product (Admin only)
* Admin Set user as admin (Admin only)
* Admin Update product info (Admin only)
* Admin get all orders (Admin only)
* Admin archive product (Admin only)